<?php
/**
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 OpenERP PHP Connector
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Odoo\Modules\Mrp;

use Odoo\Modules\Modules;
use Exception;

class Property extends Modules
{
    private $model = 'mrp.property';


    //search -> $param => [[['id', '=', 525]]]
    public function search($param = array(), $fields = array())
    {
        if (!is_array($param) && !is_array($fields)) {
            return array();
        }
        try {
            $resultRead = $this->erp->search($this->model, $param, $fields); //$fields); // return array of records
            return $resultRead;
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @param array $fields
     * @return array|null|string
     */
    public function read($id = 0, $fields = [])
    {
        if (!isset($id)) {
            return null;
        }
        try {
            $details = $this->erp->read($this->model, array($id), $fields = []);
            return $details;
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @param array $options
     * @return array|null|string
     */
    public function searchRead($id = 0, $search, $options = [])
    {
        if (!isset($id)) {
            return null;
        }
        try {
            $details = $this->erp->searchRead($this->model, $search, $options);
            return $details;
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

}
