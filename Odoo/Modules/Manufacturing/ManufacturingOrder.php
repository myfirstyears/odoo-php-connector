<?php
/**
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 OpenERP PHP Connector
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Odoo\Modules\Manufacturing;

class ManufacturingOrder extends Manufacturing
{
    private $model = 'mrp.production';

    private $allFieldListDefault = array(
        'bom_id','company_id','cycle_total','date_finished','date_planned','date_start','hour_total','location_dest_id','location_src_id','move_created_ids',
        'move_created_ids2','move_lines','move_lines2','move_prod_id','name','origin','priority','product_id','product_lines','product_qty',
        'product_uom','product_uos','product_uos_qty','progress','routing_id','state','user_id','workcenter_lines'
    );
    private $customFieldListDefault = array(
        'bom_id','company_id','cycle_total','date_finished','date_planned','date_start','hour_total','location_dest_id','location_src_id','move_created_ids',
        'move_created_ids2','move_lines','move_lines2','move_prod_id','name','origin','priority','product_id','product_lines','product_qty',
        'product_uom','product_uos','product_uos_qty','progress','routing_id','state','user_id','workcenter_lines'
    );

    public function lists($ids = array(), $fields = array())
    {
        if (!is_array($ids) && !is_array($fields)) {
            return array();
        }

        $resultRead = $this->erp->searchRead($this->model, $ids, $fields); // return array of records
        return $resultRead;
    }

    /**
     * @param $id
     * @param array $fields
     * @return null
     */
    public function realRead($id = 0, $fields = array())
    {
        if (!isset($id)) {
            return null;
        }

        if (is_array($fields) && !sizeof($fields) > 0) {
            $fields = $this->customFieldListDefault;
        }

        if ($fields == 'all') {
            $fields = $this->allFieldListDefault;
        }

        $details = $this->erp->read($this->model, array($id), $fields);
        return $details[0];
    }

    /**
     * Read used for loading by Manufacturing order ID, 'name' key in Odoo
     * @param $id
     * @param array $fields
     * @return null
     */
    public function read($id = 0, $fields = [], $options = [])
    {
        if (!isset($id)) {
            return null;
        }

        if (is_array($fields) && !sizeof($fields) > 0) {
            $fields = $this->customFieldListDefault;
        }

        if ($fields == 'all') {
            $fields = $this->allFieldListDefault;
        }

        $search = array(array(array('name', '=', $id)));

        if (count($fields) > 0) {
            $options['fields'] = $fields;
        }

        $details = $this->erp->searchRead($this->model, $search, $options);
        if (count($details) > 0) {
            return $details[0];
        }
        return [];
    }

    /**
     * Read used for loading by Manufacturing order ID, 'name' key in Odoo
     * @param $id
     * @param array $fields
     * @return null
     */
    public function readByWorkcentre($id = 0, $process = '', $fields = [], $options = [])
    {
        if (!isset($id) || $process == '') {
            return null;
        }

        if (is_array($fields) && !sizeof($fields) > 0) {
            $fields = $this->customFieldListDefault;
        }

        if ($fields == 'all') {
            $fields = $this->allFieldListDefault;
        }

        $search = array([
            ['name', '=', $id],
            ['workcenter_id', '=', $process],
        ]);

        if (count($fields) > 0) {
            $options['fields'] = $fields;
        }

        $details = $this->erp->searchRead($this->model, $search, $options);
        if (count($details) > 0) {
            return $details[0];
        }
        return [];
    }

    public function create($data = array())
    {
        $create = $this->erp->create($this->model, $data);
        return $create;
    }


    public function update($id, $data = array())
    {

    }
}
