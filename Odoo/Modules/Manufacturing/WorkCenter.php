<?php
/**
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 OpenERP PHP Connector
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Odoo\Modules\Manufacturing;

class WorkCenter extends Manufacturing
{
    private $model = 'mrp.workcenter';

    private $allFieldListDefault = array(
        'capacity_per_cycle','costs_cycle','costs_cycle_account_id','costs_general_account_id','costs_hour',
        'costs_hour_account_id','costs_journal_id','note','product_id','resource_id','time_cycle','time_start',
        'calendar_id','code','company_id','name','resource_type','time_efficiency','user_id'
    );
    private $customFieldListDefault = array(
        'capacity_per_cycle','costs_cycle','costs_cycle_account_id','costs_general_account_id','costs_hour',
        'costs_hour_account_id','costs_journal_id','note','product_id','resource_id','time_cycle','time_start',
        'calendar_id','code','company_id','name','resource_type','time_efficiency','user_id'
    );

    public function lists($ids = array(), $fields = array())
    {
        if (!is_array($ids) && !is_array($fields)) {
            return array();
        }

        $resultRead = $this->erp->searchRead($this->model, $ids = [], $fields); // return array of records
        return $resultRead;
    }

    /**
     * @param $id
     * @param array $fields
     * @return null
     */
    public function realRead($id, $fields = array())
    {
        if (!isset($id)) {
            return null;
        }

        if (is_array($fields) && !sizeof($fields) > 0) {
            $fields = $this->customFieldListDefault;
        }

        if ($fields == 'all') {
            $fields = $this->allFieldListDefault;
        }

        $details = $this->erp->read($this->model, array($id), $fields);
        return $details[0];
    }

    /**
     * Read used for loading by Work order parameters, MO ID, and
     * @param $id
     * @param array $fields
     * @return null
     */
    public function read($id = 0, $process_id = false, $fields = [], $options = [])
    {
        if (!isset($id)) {
            return null;
        }

        if (is_array($fields) && !sizeof($fields) > 0) {
            $fields = $this->customFieldListDefault;
        }

        if ($fields == 'all') {
            $fields = $this->allFieldListDefault;
        }

        $terms = [];
        if ($process_id !== false) {
            $terms[] = array('production_id', '=', $id);
            $terms[] = array('workcenter_id', '=', $process_id);
        } else {
            $terms[] = array('id', '=', $id);
        }

        $search = array($terms);

        if (count($fields) > 0) {
            $options['fields'] = $fields;
        }

        $details = $this->erp->searchRead($this->model, $search, $options);

        if (count($details) > 0) {
            return $details[0];
        }
        return [];
    }
}
