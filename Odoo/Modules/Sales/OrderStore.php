<?php
/**
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 OpenERP PHP Connector
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Odoo\Modules\Sales;

class OrderStore extends Sales
{
    private $model = 'sale.order.store';

    private $allFieldListDefault = array(
        'id'
    );
    private $customFieldListDefault = array(
        'id'
    );

    public function getProductPrice($channel_id, $id, $options = array())
    {
        return $this->erp->get_product_price($this->model, $channel_id, $id, $options); // return array of records
    }

    public function getListOfCouponCodesPerChannel($ids = array(), $fields = array())
    {
        if (!is_array($ids) && !is_array($fields)) {
            return array();
        }

        $resultRead = $this->erp->get_list_of_coupon_codes_per_channel($this->model, $ids, $fields); // return array of records
        return $resultRead;
    }

    public function getCouponCodeData($ids = array(), $fields = array())
    {
        if (!is_array($ids) && !is_array($fields)) {
            return array();
        }

        $resultRead = $this->erp->get_coupon_code_data($this->model, $ids, $fields); // return array of records
        return $resultRead;
    }

    public function getListOfGiftCertificates($ids = array(), $fields = array())
    {
        if (!is_array($ids) && !is_array($fields)) {
            return array();
        }

        $resultRead = $this->erp->get_list_of_gift_certificates($this->model, $ids, $fields); // return array of records
        return $resultRead;
    }

    public function getGiftCertificateData($ids = array(), $fields = array())
    {
        if (!is_array($ids) && !is_array($fields)) {
            return array();
        }

        $resultRead = $this->erp->get_gift_certificate_data($this->model, $ids, $fields); // return array of records
        return $resultRead;
    }
    //

    public function getListOfProductsPerChannel($ids = array(), $fields = array())
    {
        if (!is_array($ids) && !is_array($fields)) {
            return array();
        }

        $resultRead = $this->erp->get_list_of_products_per_channel($this->model, [ $ids ], $fields); // return array of records
        return $resultRead;
    }

    public function getProductDataPerChannel($id = false, $sku = false, $fields = [])
    {
        if ($id === false && $sku == false) {
            return array();
        }

        $resultRead = $this->erp->getProductDataPerChannel($this->model, $id, $sku, $fields); // return array of records
        return $resultRead;
    }

    public function lists($ids = array(), $fields = array())
    {
        if (!is_array($ids) && !is_array($fields)) {
            return array();
        }

        $resultRead = $this->erp->searchRead($this->model, $ids, $fields); // return array of records
        return $resultRead;
    }

    /**
     * @param $id
     * @param array $fields
     * @return null
     */
    public function realRead($id = 0, $fields = array())
    {
        if (!isset($id)) {
            return null;
        }

        if (is_array($fields) && !sizeof($fields) > 0) {
            $fields = $this->customFieldListDefault;
        }

        if ($fields == 'all') {
            $fields = $this->allFieldListDefault;
        }

        $details = $this->erp->read($this->model, array($id), $fields);
        return $details[0];
    }

    /**
     * Read used for loading by Sales order ID, 'name' key in Odoo
     * @param $id
     * @param array $fields
     * @return null
     */
    public function read($id = 0, $fields = [], $options = [])
    {
        if (!isset($id)) {
            return null;
        }


        if (is_array($fields) && !sizeof($fields) > 0) {
            $fields = $this->customFieldListDefault;
        }

        if ($fields == 'all') {
            $fields = $this->allFieldListDefault;
        }

        $search = array(array(array('name', '=', $id)));

        if (count($fields) > 0) {
            //$options['fields'] = $fields;
        }

        $details = $this->erp->searchRead($this->model, $search, $options);
        if (count($details) > 0) {
            return $details[0];
        }
        return [];
    }

    /**
     * Post message for a sales order
     * @param $id
     * @param array $fields
     * @return mixed
     */
    public function postMessage($id, $message = '', $options = [])
    {
        if (!isset($id)) {
            return false;
        }

        if ($message === '') {
            return false;
        }


        $data = $this->read($id);
        $so_id = $data['id'];

        $data = array(
            'body' => $message,
            'type' => 'comment',
            //'res_id' => $so_id,
            //'model' => $this->model,
            'visible_by_operator' => true,  // toggle for visible by operator
        );
        
        //$details = $this->erp->create('mail.message', $data);
        $details = $this->erp->messagePost('sale.order', [[$so_id]], $data);

        return $details;
    }
    /**
     * get messages for a sales order
     *
     * @param $id
     * @param array $fields
     * @return mixed
     */
    public function getMessages($id, $options = [])
    {
        if (!isset($id)) {
            return false;
        }

        $data = $this->read($id);
        $so_id = $data['id'];

        $search = array(array(
            array('res_id', '=', $so_id),
            array('model', '=', $this->model),
            array('visible_by_operator', '=', true),  // toggle for visible by operator
        ));
        
        $details = $this->erp->searchRead('mail.message', $search, $options);
        
        return $details;
    }
    /**
     * creates a transfer for a sales order
     *
     * @param $id
     * @param array $fields
     * @return mixed
     */
    public function transferPacking($id, $options = [])
    {
        if (!isset($id)) {
            return false;
        }

        $data = $this->read($id);
        $so_id = $data['id'];

        $details = $this->erp->transfer_packing($this->model, [$so_id]);

        return $details;
    }

    public function create(array $data) : array
    {
        if(empty($data)) {
            return array();
        }
        $create = $this->erp->create($this->model, $data);
        return $create;
    }


    public function update($id, $data = array())
    {

    }

    //return only id
    public function search(array  $search, int $offset = 0, int $limit = 1000) : array
    {
        //search example $search = [[['code', '=', 'john_lewis']]]
        if(empty($search)){
            return array();
        }
        $resultRead = $this->erp->search($this->model, $search, []);
        return $resultRead;
    }
    //get list of category
    public function getListOfCategories(array $categoryId = []) : array
    {
        $result = $this->erp->executeFunction($this->model, 'get_list_of_categories', $categoryId);
        return $result;
    }
    //get list of channels
    public function getListOfChannels(array $channelId = [], array $options = []) : array
    {
        $result = $this->erp->executeFunction($this->model, 'get_list_of_channels', $channelId, $options);
        return $result;
    }
    //get list of product per channel
    public function getProductsPerChannel(array $ids = [], array $options = []) : array
    {
        $result = $this->erp->executeFunction($this->model, 'get_list_of_products_per_channel', $ids, $options);
        return $result;
    }
    //get product data per channel
    public function productPerChannel(int $id = null, string $sku = '') : array
    {
        $result = $this->erp->executeFunction($this->model, 'get_product_data_per_channel', $id, $sku);
        return $result;
    }
}
