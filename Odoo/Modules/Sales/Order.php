<?php
/**
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 OpenERP PHP Connector
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Odoo\Modules\Sales;

class Order extends Sales
{
    private $model = 'sale.order';

    private $allFieldListDefault = array(
        'amount_tax','amount_total','amount_untaxed','client_order_ref','company_id','create_date','currency_id',
        'date_confirm','date_order','fiscal_position','invoice_exists','invoice_ids',
        'invoiced','invoiced_rate','name','note','order_line','order_policy','origin','partner_id','partner_invoice_id',
        'partner_shipping_id','payment_term','pricelist_id','project_id','state','user_id'
    );
    private $customFieldListDefault = array(
        'amount_tax','amount_total','amount_untaxed','client_order_ref','company_id','create_date','currency_id',
        'date_confirm','date_order','fiscal_position','invoice_exists','invoice_ids',
        'invoiced','invoiced_rate','name','note','order_line','order_policy','origin','partner_id','partner_invoice_id',
        'partner_shipping_id','payment_term','pricelist_id','project_id','state','user_id'
    );

    //create sale order
    public function createOrder($parameter)
    {
        if(!empty($parameter)) {
            $parameter = json_encode($parameter);
        } else {
            return array();
        }
        $result = $this->erp->execute($this->model, 'create_sale_order', $parameter);
        return $result;
    }

    //cancelled order
    public function cancelledOrder(int $orderId)
    {
        $result = $this->erp->execute($this->model, 'action_advanced_cancel', $orderId);
        return $result;
    }

    public function lists($option = array(), $fields = array())
    {
        //example $option = array(array(array('name', '=', 'SO2087')))
        //example $fields = array('fields'=>array('id'), 'limit'=>5)
        if (!is_array($option) && !is_array($fields)) {
            return array();
        }
        $resultRead = $this->erp->searchRead($this->model, $option, $fields); // return array of records
        return $resultRead;
    }

    /**
     * @param $id
     * @param array $fields
     * @return null
     */
    public function realRead($id = 0, $fields = array())
    {
        if (!isset($id)) {
            return null;
        }

        if (is_array($fields) && !sizeof($fields) > 0) {
            $fields = $this->customFieldListDefault;
        }

        if ($fields == 'all') {
            $fields = $this->allFieldListDefault;
        }

        $details = $this->erp->read($this->model, array($id), $fields);
        return $details[0];
    }

    /**
     * Read used for loading by Sales order ID, 'name' key in Odoo
     * @param $id
     * @param array $fields
     * @return null
     */
    public function read($id = 0, $fields = [], $options = [])
    {
        if (!isset($id)) {
            return null;
        }


        if (is_array($fields) && !sizeof($fields) > 0) {
            $fields = $this->customFieldListDefault;
        }

        if ($fields == 'all') {
            $fields = $this->allFieldListDefault;
        }

        $search = array(array(array('name', '=', $id)));

        if (count($fields) > 0) {
            $options['fields'] = $fields;
        }

        $details = $this->erp->searchRead($this->model, $search, $options);
        if (count($details) > 0) {
            return $details[0];
        }
        return [];
    }

    /**
     * Post message for a sales order
     * @param $id
     * @param array $fields
     * @return mixed
     */
    public function postMessage($id, $message = '', $options = [])
    {
        if (!isset($id)) {
            return false;
        }

        if ($message === '') {
            return false;
        }


        $data = $this->read($id);
        $so_id = $data['id'];

        $data = array(
            'body' => $message,
            'type' => 'comment',
            //'res_id' => $so_id,
            //'model' => $this->model,
            'visible_by_operator' => true,  // toggle for visible by operator
        );
        
        //$details = $this->erp->create('mail.message', $data);
        $details = $this->erp->messagePost('sale.order', [[$so_id]], $data);

        return $details;
    }
    /**
     * get messages for a sales order
     *
     * @param $id
     * @param array $fields
     * @return mixed
     */
    public function getMessages($id, $options = [])
    {
        if (!isset($id)) {
            return false;
        }

        $data = $this->read($id);
        $so_id = $data['id'];

        $search = array(array(
            array('res_id', '=', $so_id),
            array('model', '=', $this->model),
            array('visible_by_operator', '=', true),  // toggle for visible by operator
        ));
        
        $details = $this->erp->searchRead('mail.message', $search, $options);
        
        return $details;
    }
    /**
     * creates a transfer for a sales order
     *
     * @param $id
     * @param array $fields
     * @return mixed
     */
    public function transferPacking($id, $options = [])
    {
        if (!isset($id)) {
            return false;
        }

        $data = $this->read($id);
        $so_id = $data['id'];

        $details = $this->erp->transfer_packing($this->model, [$so_id]);

        return $details;
    }

    public function create($data = array())
    {
        $create = $this->erp->create($this->model, $data);
        return $create;
    }

    public function update($id, $data = array())
    {

    }

    //return only id
    public function search(array  $search, int $offset = 0, int $limit = 1000) : array
    {
        //search example $search = [[['name', '=', '17 m2 haider']]]
        if (empty($search)) {
            return array();
        }
        $resultRead = $this->erp->search($this->model, $search, []);
        return $resultRead;
    }

    public function orderConfirm($nameOrId)
    {
        if (is_numeric($nameOrId)) {
            $soId = $nameOrId;
        } elseif (is_string($nameOrId)) {
            $searchResult = $this->search([[['name', '=', $nameOrId]]]);
            if (!empty($searchResult)) {
                $soId = $searchResult[0];
            } else {
                $soId = '';
            }
        } else {
            return array();
        }
        $result = $this->erp->execWorkflow($this->model, 'order_confirm', $soId);
        return $result;

    }
    //close invoices
    public function manualCloseInvoices($nameOrId) : array
    {
        if (is_numeric($nameOrId)) {
            $soId = $nameOrId;
        } elseif (is_string($nameOrId)) {
            $searchResult = $this->search([[['name', '=', $nameOrId]]]);
            if (!empty($searchResult)) {
                $soId = $searchResult[0];
            } else {
                $soId = '';
            }
        } else {
            return array('result' => false);
        }

        $result = $this->erp->executeFunction($this->model, 'manual_close_invoices', $soId);
        return array('result' => $result);
    }


    // new order sync method
    public function sync($data)
    {

        return $this->erp->executeFunction($this->model, 'create_or_update_sale_order', $data);
    }
}
