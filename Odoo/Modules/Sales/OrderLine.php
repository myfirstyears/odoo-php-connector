<?php
/**
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 OpenERP PHP Connector
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Odoo\Modules\Sales;

class OrderLine extends Sales
{
    private $model = 'sale.order.line';
    //return only id
    public function search(array  $search, int $offset = 0, int $limit = 1000) : array
    {
        //search example $search = [[['order_id', '=', 91]]]
        if (empty($search)) {
            return array();
        }
        $resultRead = $this->erp->search($this->model, $search, []);
        return $resultRead;
    }
    // return full
    public function searchRead(array  $search, $options = []) : array
    {
        //search example $search = [[['order_id', '=', 91]]]
        if (empty($search)) {
            return array();
        }
        $resultRead = $this->erp->searchRead($this->model, $search, $options);
        return $resultRead;
    }
    // get list of personalization options
    public function getPersonalizations(array  $search) : array
    {
        //search example $search = [[['order_id', '=', 91]]]
        $sol = $this->search($search);
        $resultRead = $this->erp->execute($this->model, 'get_printable_names_values', [$sol[0]]);
        return $resultRead;
    }
    //delete records
    public function unlink($option)
    {
        //option example $option = [[['id', '=', 34]]];
        if (empty($option)) {
            return array();
        }
        $resultRead = $this->erp->unlink($this->model, $option);
        return $resultRead;
    }
    //create option value
    public function create(array $data)
    {
        if (empty($data)) {
            return array();
        }
        $create = $this->erp->create($this->model, $data);
        return $create;
    }
}
