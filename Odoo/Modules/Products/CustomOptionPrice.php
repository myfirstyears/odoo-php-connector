<?php
/**
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 OpenERP PHP Connector
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Odoo\Modules\Products;

class CustomOptionPrice extends Products
{
    private $model = 'product.custom_option.price';


    public function fields($options = array())
    {
        if (!is_array($options)) {
            return array();
        }

        $resultRead = $this->erp->fields_get($this->model, [], $options); // return array of records
        return $resultRead;
    }

    //search
    public function search(array  $search, int $offset = 0, int $limit = 1000) : array
    {
        if(empty($search)){
            return array();
        }

        $resultRead = $this->erp->search($this->model, $search, [], $offset, $limit);
        return $resultRead;
    }
    //search and return details
    public function searchRead(array  $search = [], array $option = [], int $offset = 0, int $limit = 1000) : array
    {
        if(empty($search)){
            return array();
        }

        $resultRead = $this->erp->searchRead($this->model, $search, $option, $offset, $limit);
        return $resultRead;
    }

    public function realRead(int $id , array $options = array()) : array
    {
        if (!isset($id)) {
            return null;
        }

        $details = $this->erp->read($this->model, array($id), $options);
        return $details;
    }

    public function lists(array $ids = array(), array $option = array()) : array
    {
        if (!is_array($ids) && !is_array($option)) {
            return array();
        }

        $resultRead = $this->erp->searchRead($this->model, $ids, $option); // return array of records
        return $resultRead;
    }
    //create option value
    public function create(array $data)
    {
        if(empty($data)){
            return array();
        }
        $create = $this->erp->create($this->model, $data);
        return $create;
    }

}
