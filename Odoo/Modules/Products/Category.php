<?php
/**
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 OpenERP PHP Connector
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Odoo\Modules\Products;

class Category extends Products
{
    private $model = 'product.category';

    private $allFieldListDefault = array(
        'create_date','sequence','property_stock_account_input_categ','write_uid','total_route_ids','child_id','property_stock_valuation_account_id','id','property_stock_account_output_categ','removal_strategy_id','create_uid','property_account_creditor_price_difference_categ','display_name','__last_update','parent_id','complete_name','type','property_account_expense_categ','property_stock_journal','channel_ids','route_ids','write_date','property_account_income_categ','parent_right','name','parent_left','magento_bind_ids'
    );
    private $customFieldListDefault = array(
        'create_date','sequence','child_id','create_uid','display_name','__last_update','parent_id','complete_name','type','channel_ids','write_date','parent_right','name','parent_left','magento_bind_ids'
    );

    public function fields($options = array())
    {
        if (!is_array($options)) {
            return array();
        }

        $resultRead = $this->erp->fields_get($this->model, [], $options); // return array of records
        return $resultRead;
    }

    public function lists($ids = array(), $options = [])
    {
        if (!is_array($ids) && !is_array($options)) {
            return array();
        }

        //set needed Odoo fields
        $options['fields'] = $this->setOdooFields($options, []/*$this->customFieldListDefault*/);

        $resultRead = $this->erp->searchRead($this->model, $ids, $options); // return array of records
        return $resultRead;
    }

    /**
     * @param $id
     * @param array $fields
     * @return null
     */
    public function realRead($id, $fields = array())
    {
        if (!isset($id)) {
            return null;
        }

        if (is_array($fields) && !sizeof($fields) > 0) {
            $fields = $this->customFieldListDefault;
        }

        if ($fields == 'all') {
            $fields = $this->allFieldListDefault;
        }

        $details = $this->erp->read($this->model, array($id), $fields);
        return $details[0];
    }

    /**
     * Read used for loading by Work order parameters, MO ID, and
     * @param $id
     * @param array $fields
     * @return null
     */
    public function read($id = 0, $fields = [], $options = [])
    {
        if (!isset($id)) {
            return null;
        }

        if (is_array($fields) && !sizeof($fields) > 0) {
            $fields = $this->customFieldListDefault;
        }

        if ($fields == 'all') {
            $fields = $this->allFieldListDefault;
        }

        $terms = [];
        $terms[] = array('name', '=', $id);

        $search = array($terms);

        if (count($fields) > 0) {
            $options['fields'] = $fields;
        }

        $details = $this->erp->searchRead($this->model, $search, $options);

        if (count($details) > 0) {
            return $details[0];
        }
        return [];
    }
}
