<?php
/**
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 OpenERP PHP Connector
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Odoo\Modules\Products;

class Channel extends Products
{
    private $model = 'product.channel';

    private $allFieldListDefault = [
        'name', 'description', 'channel_price', 'channel_tax', 'link', 'sort_order'
    ];
    private $customFieldListDefault = [
        'name', 'description', 'channel_price', 'channel_tax', 'link', 'sort_order'
    ];

    public function lists($ids = array(), $fields = array())
    {
        if (!is_array($ids) && !is_array($fields)) {
            return array();
        }

        $resultRead = $this->erp->searchRead($this->model, $ids, $fields); // return array of records
        return $resultRead;
    }

    /**
     * @param $id
     * @param array $fields
     * @return null
     */
    public function realRead($id = 0, $fields = [])
    {
        if (!isset($id)) {
            return null;
        }

        if (is_array($fields) && !sizeof($fields) > 0) {
            $fields = $this->customFieldListDefault;
        }

        if ($fields == 'all') {
            $fields = $this->allFieldListDefault;
        }

        $details = $this->erp->read($this->model, array($id), $fields);
        return $details[0];
    }

    /**
     * Read used for loading by Sku order ID, 'sku' key in Odoo
     * @param $id
     * @param array $fields
     * @return null
     */
    public function read($id = 0, $fields = [], $options = [])
    {
        if (!isset($id)) {
            return null;
        }

        if (is_array($fields) && !sizeof($fields) > 0) {
            $fields = $this->customFieldListDefault;
        }

        if ($fields == 'all') {
            $fields = $this->allFieldListDefault;
        }

        $search = array(array(array('sku', '=', $id)));

        if (count($fields) > 0) {
            $options['fields'] = $fields;
        }

        $details = $this->erp->searchRead($this->model, $search, $options);
        if (count($details) > 0) {
            return $details[0];
        }
        return [];
    }

    public function create($data = array())
    {
        //$create = $this->erp->create($this->model, $data);
        //return $create;
    }


    public function update($id, $data = array())
    {

    }
}
