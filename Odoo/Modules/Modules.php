<?php
/**
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 OpenERP PHP Connector
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Odoo\Modules;

use Odoo\Odoo;

class Modules
{
    public $erp;
    private $server;
    private $database;
    private $username;
    private $password;

    /**
     * @param $username
     * @param $password
     * @param $database
     * @param $server
     */
    public function __construct($server, $database, $username = false, $password = false)
    {
        $this->erp = Odoo::instance($server, $database, $username, $password);
    }

    public static function getModelAllFields($result)
    {
        $results = array_keys($result);
        $variable = 'array(';
        foreach ($results as $key => $value) {
            $variable .= '\''.$value.'\''.', ';
        }
        $variable .= ')';
        return $variable;
    }

    /**
     * Suppose the return array has country_id => array[data]=> array[value] => [0 => '', 1 => 'Original Value']
     * In this situatio we can easily extract the exact data. I am lazy so I made this handy function.
     *
     * @param $key
     * @return mixed
     */
    protected function getValueFromKey($key)
    {
        foreach ($key['data']['value'][1] as $key => $value) {
            return $value;
        }
    }

    //set needed Odoo fields
    protected function setOdooFields($options, $customFields)
    {
        $fields = [];
        if(isset($options['fields'])) {
            if(!empty($options['fields'])) {
                if(isset($options['fields'][0])) {
                    if (!empty($options['fields'][0])) {
                        if ($options['fields'][0] == 'all') {
                            return $fields;
                        } else {
                            return $options['fields'];
                        }
                    } else {
                        return $fields;
                    }
                }
            }
            if ($options['fields'] === []) {
                return $customFields;
            }
        } else {
            return $customFields;
        }
        return $fields;
    }
}
