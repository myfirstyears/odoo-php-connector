<?php
/**
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 OpenERP PHP Connector
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Odoo\Modules\HumanResource;

class Attendance extends HumanResource
{
    /**
     * @param $employeeId
     * @param $action
     * @param string $actionDesc
     * @param $time
     * @return bool|int
     */
    public function add($employeeId, $action, $actionDesc = '', $time = false)
    {
        if ($time === false) {
            $time = time();
        }
        if (!in_array($action, array('sign_in', 'sign_out'))) {
            return false;
        }

        date_default_timezone_set('UTC');

        $response = $this->erp->create('hr.attendance', array('employee_id' => $employeeId, 'action' => $action, 'action_desc' => $actionDesc, 'name' => $time));
        return $response;
    }

    public function lists($limit = 1000, $offset = 0)
    {

    }

    public function readAttendance($employeeId, $conditions = array(), $limit = 100, $offset = 0)
    {

    }

    public function searchAttendance($employeeId, $dateTime)
    {

    }
}
