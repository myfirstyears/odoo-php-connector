<?php
/**
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 OpenERP PHP Connector
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *
 *
 * renamed methods to prevent following (http://stackoverflow.com/questions/19592550/declaration-of-method-must-be-compatible-with-that-of-interface-method-when-me)
 *
 *
 */

namespace Odoo\Modules\HumanResource;

class Employee extends HumanResource
{
    private $model = 'hr.employee';

    protected $allFieldListDefault = array('address_home_id','address_id','bank_account_id','birthday','category_ids','child_ids','city','coach_id','color','country_id','department_id','gender','identification_id','image','image_medium','image_small','job_id','last_login','login','marital','Mobile_phone','name_related','notes','otherid','parent_id','passport_id','resource_id','sinid','ssnid','work_email','work_location','work_phone');

    protected $customFieldListDefault = array('address_home_id','address_id','bank_account_id','birthday','category_ids','child_ids','city','coach_id','color','country_id','department_id','gender','identification_id','image','image_medium','image_small','job_id','last_login','login','marital','Mobile_phone','name_related','notes','otherid','parent_id','passport_id','resource_id','sinid','ssnid','work_email','work_location','work_phone');

    public function lists($ids = array(), $fields = array())
    {
        if (!is_array($ids) && !is_array($fields)) {
            return array();
        }

        $resultRead = $this->erp->searchRead($this->model, $ids, ['fields' => ['name','department_id','work_email']]); // return array of records
        return $resultRead;
    }

    /**
     * Get details of a specific user
     *
     * @param $id
     * @param array $fields
     * @return null
     */
    public function read($id, $fields = array())
    {
        if (!isset($id)) {
            return null;
        }

        if (!sizeof($fields) > 0) {
            $fields = $this->allFieldListDefault;
        }

        $details = $this->erp->read($this->model, array($id), $fields);
        return $details[0];
    }

    public function create($data = array())
    {
        return null;
    }

    public function search($criteria = [], $options = [], $offset = 0, $limit = 1000)
    {

        $employees = $this->erp->searchRead($this->model, [$criteria], $options, $offset, $limit);

        if (sizeof($employees) > 0) {
            return $employees;
        }

        return null;
    }

    public function update($id, $data = array())
    {

    }

    public function readByLoginId($loginId, $fields = array())
    {
        $employees = $this->search([['login', '=', $loginId]]);

        if (count($employees) > 0) {
            return $employees[0];
        }
        return null;
    }
}
