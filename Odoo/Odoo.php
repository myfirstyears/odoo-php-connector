<?php
/**
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 OpenERP PHP Connector
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace Odoo;

//use Odoo\Client\XmlRpc\XmlRpc;
use Ripcord\Ripcord;

/**
 * Class Odoo
 * @package Odoo\Odoo
 */
class Odoo
{
    /**
     * @var
     */
    //private $_defaultPath = '';

    /**
     * @var
     */
    private $_uid;

    /**
     * @var
     */
    private $_version = 0.1;

    /**
     * @var
     */
    private $_db;

    /**
     * @var
     */
    //private $_login;

    /**
     * @var
     */
    private $_password;

    /**
     * @var
     */
    private $_host;


    /**
     *
     */
    public static function instance($host, $database, $username = false, $password = false)
    {
        if (!isset($_SESSION['odoo_client'])) {
            $_SESSION['odoo_client'] = new self($host, $database, $username, $password);
        }

        return $_SESSION['odoo_client'];
    }


    /**
     * @param $host
     * @param string $charset
     */
    public function __construct($host, $database, $username = false, $password = false)
    {
        $this->_host = $host;
        $this->_db = $database;

        return $this->login($username, $password);
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->_uid;
    }

    /**
     * @param $db
     * @param $login
     * @param $password
     * @return mixed (int (uid) if success, bool(false) on error
     */
    public function login($login, $password)
    {
        //$this->_login = $login;
        $this->_password = $password;
        $url = $this->_host;
        $db = $this->_db;

        $common = Ripcord::client($url."/xmlrpc/2/common");

        $response = $common->authenticate($db, $login, $password, array());

        if ($response !== false) {
            $this->_uid = $response;

        } else {
            $this->throwExceptionIfFault('Login Failed: Check credentials');

        }

        return $response;
    }

    /**
     * @param $db
     * @param $login
     * @param $password
     * @return mixed (int (uid) if success, bool(false) on error
     */
    public function logout()
    {
        //$this->_login = $login;
        unset($this->_password);
        unset($this->_uid);

        return $this;
    }

    /**
     * @return mixed
     */
    public function version()
    {
        $common = Ripcord::client($this->_host."/xmlrpc/2/common");

        $response = $common->version();

        if ($response !== false) {
            $this->_version = $response;
        } else {
            $this->throwExceptionIfFault('Version check failed');
        }

        return $response;
    }

    /**
     * @param bool $extended
     * @return mixed
     */
    public function about($extended = false)
    {

        $common = Ripcord::client($this->_host."/xmlrpc/2/common");

        $response = $common->about();

        if ($response === false) {
            $this->throwExceptionIfFault('About check failed');
        }

        return $response;
    }

    /**
     * @param null $db
     * @param null $login
     * @param null $password
     * @return mixed
     */
    public function getTimezone()
    {

        $common = Ripcord::client($this->_host."/xmlrpc/2/common");

        $response = $common->timezone_get();

        if ($response === false) {
            $this->throwExceptionIfFault('Timezone check failed');
        }

        return $response;
    }

    /**
     * @param $model
     * @param $data
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function search($model, $param, $options = [], $offset = 0, $limit = 1000)
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");

        $options['offset'] = $offset;
        $options['limit'] = $limit;

        if ($model == 'hr.employee') {
            echo '<pre>';
            print_r([$this->_db, $this->_uid, $this->_password, $model, 'search', $param, $options]);
            echo '</pre>';
        }

        $response = $objects->execute_kw(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            'search',
            $param,
            $options
        );

        if ($response === false) {
            $this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }

    /**
     * @param $model
     * @param $ids
     * @param array $fields
     * @return array
     */
    public function read($model, $data, $options = [])
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");

        $response = $objects->execute_kw(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            'read',
            $data,
            $options
        );

        if ($response === false) {
            $this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }

    /**
     * @param $model
     * @param $data
     * @return int
     */
    public function create($model, $data)
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");

        $response = $objects->execute_kw(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            'create',
            $data
        );

        if ($response === false) {
            $this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }

    /**
     * @param $model
     * @param $ids
     * @param $fields
     * @return bool|mixed|\SimpleXMLElement|string
     */
    public function write($model, $data, $options = [])
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");

        $response = $objects->execute_kw(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            'write',
            $data,
            $options
        );

        if ($response === false) {
            $this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }

    /**
     * @param $model
     * @param $data
     * @return bool|mixed|\SimpleXMLElement|string
     */
    public function unlink($model, $data)
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");

        $response = $objects->execute_kw(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            'unlink',
            $data
        );

        if ($response === false) {
            $this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }

    //Loads different collections function
    public function execute($collection, $function, $data)
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");
        $response = $objects->execute_kw(
            $this->_db,
            $this->_uid,
            $this->_password,
            $collection,
            $function,
            [$data]
        );
        return $response;
    }
    
    /**
     * @param $model
     * @param $signal
     * @param $data
     * @return bool|mixed|\SimpleXMLElement|string
     * @throws \Exception
     */
    public function execWorkflow($model, $signal, $id)
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");

        $response = $objects->execute(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            $signal,
            $id
        );

        if ($response === false) {
            //$this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }

    /**
     * @param $model
     * @param $data
     * @return int|null
     */
    public function searchCount($model, $data, $options = array())
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");

        $response = $objects->execute_kw(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            'search_count',
            $data,
            $options
        );

        if ($response === false) {
            $this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }

    /**
     * @param $model
     * @param array $data
     * @return mixed
     */
    public function getCount($model, $data = array())
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");
        $response = $objects->execute_kw(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            'search_count',
            [$data]
        );

        if ($response === false) {
            $this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }
    
    /**
     * @param $model
     * @param $data
     * @param array $fields
     * @param int $offset
     * @param int $limit
     * @return array
     * @throws \Exception
     */
    public function searchRead($model, $data, $options = [])
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");


        $response = $objects->execute_kw(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            'search_read',
            $data,
            $options
        );

        if ($response === false) {
            $this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }

    //this method use when product import from Odoo
    public function importSearchRead($model, $data, $options = [], $offset = 0, $limit = 100)
    {
        try {
            $objects = Ripcord::client($this->_host . "/xmlrpc/2/object");
            $options['offset'] = $offset;
            $options['limit'] = $limit;
            $response = $objects->execute_kw(
                $this->_db,
                $this->_uid,
                $this->_password,
                $model,
                'search_read',
                $data,
                $options
            );
            if ($response === false) {
                $response = [];
            }
        } catch(\Exception $e){
            $response = [];
        }
        return $response;
    }
    
    /**
     * @param $model
     * @param $data
     * @param array $fields
     * @param int $offset
     * @param int $limit
     * @return array
     * @throws \Exception
     */
    public function messagePost($model, $data, $options = [], $offset = 0, $limit = 100)
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");

        //$options['offset'] = $offset;
        //$options['limit'] = $limit;

        $response = $objects->execute_kw(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            'message_post',
            $data,
            $options
        );

        if ($response === false) {
            $this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }

    public function executeFunction($model, $rel, $id = [], $options = [], $offset = 0, $limit = 100)
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");

        //$options['offset'] = $offset;
        //$options['limit'] = $limit;

        $response = $objects->execute(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            $rel,
            $id,
            $options
        );

        if ($response === false) {
            $this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }

    /**
     * @param $model
     * @param $methodParam
     * @param $options
     * @return mixed
     * get specific price by sku and stock code
     */
    public function getSpecialPrice($model, $methodParam, $options)
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");

        $response = $objects->execute_kw(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            'get_special_price',
            $methodParam,
            $options
        );

        if ($response === false) {
            $this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }
    
    /**
     * @param $model
     * @param $data
     * @param array $fields
     * @param int $offset
     * @param int $limit
     * @return array
     * @throws \Exception
     */
    public function getProductDataPerChannel($model, $id, $sku, $options = [], $offset = 0, $limit = 100)
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");

        //$options['offset'] = $offset;
        //$options['limit'] = $limit;

        $response = $objects->execute(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            'get_product_data_per_channel',
            $id,
            $sku,
            $options
        );

        if ($response === false) {
            $this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }
    
    /**
     * @param $model
     * @param $data
     * @param array $fields
     * @param int $offset
     * @param int $limit
     * @return array
     * @throws \Exception
     */
    public function get_product_price($model, $channel_id, $id, $options = [])
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");

        //$options['offset'] = $offset;
        //$options['limit'] = $limit;

        $response = $objects->execute(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            'get_product_price',
            $channel_id,
            $id,
            $options
        );

        if ($response === false) {
            $this->throwExceptionIfFault(__FUNCTION__.' failed on model: '.$model);
        }

        return $response;
    }

    /**
     * @param $response
     * @throws \Exception
    */
    public function throwExceptionIfFault($response)
    {
        if (isset($response['fault'])) {
            $faultArray = $response['fault']['value']['struct']['member'];
            $faultCode = 0;
            $faultString = 'Undefined fault string';

            foreach ($faultArray as $fault) {
                if ($fault['name'] == 'faultCode') {
                    $f = $fault['value'];
                    if (isset($f['string'])) {
                        $faultCode = 0;
                        $faultString = $f['string'];
                        break;
                    }
                    if (isset($f['int'])) {
                        $faultCode = $f['int'];
                    }
                }
                if ($fault['name'] == 'faultString') {
                    $faultString = $fault['value']['string'];
                }
            }

            throw new \Exception($faultString, $faultCode);
        } else if (is_string($response)) {
            throw new \Exception($response);
        } else {
            throw new Exception("Odoo RPC Ded");
        }
    }

    /**
     * For non standard or model specific requests
     * @param $name
     * @param $arguments
     * @return execution of api call
     */
    public function __call($name, $arguments)
    {
        $objects = Ripcord::client($this->_host."/xmlrpc/2/object");

        // lets make sure arguments are okay
        if (!isset($arguments[0]) || !is_string($arguments[0])) {
            $this->throwExceptionIfFault('Cant Query: Invalid Model "'.$arguments[0].'"');
        }

        if (!isset($arguments[1]) || !is_array($arguments[1])) {
            $this->throwExceptionIfFault('Cant Query: Invalid Options for model "'.$arguments[0].'"');
        }

        if (isset($arguments[2]) && !is_array($arguments[2])) {
            $this->throwExceptionIfFault('Cant Query: Invalid Model "'.$arguments[0].'"');
        }

        $model = $arguments[0];
        $data = $arguments[1];
        $options = isset($arguments[2]) ? $arguments[2] : [];

        $response = $objects->execute_kw(
            $this->_db,
            $this->_uid,
            $this->_password,
            $model,
            $name,
            $data,
            $options
        );

        if ($response === false) {
            $this->throwExceptionIfFault('Dynamic method '. $name .' failed on model: '.$model);
        }

        return $response;
    }
}
