![OpenErp Logo](https://sigrhe.dgae.mec.pt/openerp/static/images/openerp_small.png)

Odoo PHP Connector
=====================

Connect your PHP driven web application with Odoo through PHP XML RPC

#####Installation & Usage
To get started you need to install this library via **[Composer](http://getcomposer.org)**. If you have ready **composer.json** file then just add `"novocast/odoo-php-connector": "dev-master"` as a dependency. Or if you don't have any composer.json ready then copy/paste the below code and create your **composer.json** file.

```json
{
    "name": "YourProjectName",

    "description": "YourProjectDescription",

    "require": {
        "php": ">=5.3.0",
        "novocast/odoo-php-connector": "dev-master"
    }
}
```
Now just run `composer update` command and this **odoo php connector** library will be cloned into your **vendor** directory.


Now I am assuming that you already cloned this repository via composer.json or direct clone from GitHub. Now to use this you have to write down the following codes on your script.

```php
require_once 'vendor/autoload.php';

use Odoo\Modules\Sales\Customer;

/**
* USERNAME = Your Odoo username. i.e: admin
* PASSWORD = Your Odoo password. i.e: 123456
* DATABASE = Your Odoo database name. i.e: openerp_demo
* SERVER = Your Odoo Server. i.e: http://yourOdooServer.com
*/
$sales = new Customer(USERNAME, PASSWORD, DATABASE, SERVER);
$result = $sales->read($customerID);   // a customer ID to get details from your Odoo. i.e: 10
var_dump($result); die();
```

Now it will give you a PHP array. And now you can use the data however you want.

- [Submit Issues](https://github.com/novocast/odoo-php-connector/issues/new)
  Submit issues if you found anything wrong or face any problem to use this library
- [Instant Support](mailto:shaharia@previewict.com)
- [Documentation/Wiki](https://github.com/novocast/odoo-php-connector/wiki)

Please [Form](https://github.com/novocast/odoo-php-connector/fork) and send me Pull request if you added something useful for this. Happy PHP-ing!
